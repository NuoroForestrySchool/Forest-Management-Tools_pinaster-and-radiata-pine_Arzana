# Forest Management Tools - (maritime- and radiata-pine Arzana, Sardegna)

The projects aim is to develop taper functions and growth & yield estimation models.  
The repo will progressively grow as new reserach is developed.
Initial effort is devoted to pine plantations in Sardegna.
Current work is oncentrated on a plantation of the 1970s, property of Arzana municipality, with Pinus pinaster and P. radiata.
The area (11.5 ha) has been fully tallied, with a schematic representation of the spatial distribution of the trees.
A stratified sample of stems (90 elements) has been felled and measured to the top, on a sub sample (30 stems) stem analysis disks have been cut and stored.  
Data repository: "ForestManagementTools_2017_Pinus-plantation_Arzana_survey" [https://drive.google.com/open?id=12LVSdvcV_2PX2Koh0PrpXIHOjvpQgWJS]

## A) Data acquisition, verification and archival as relational DB

First step of the project concerns the structuring and population of the data-base.  
A basic ER-schema has been drawn analysing information requirements.  
Forest measurements have been registered on paper forms, structured in order to faciltate the tally while respecting schema requirements. 
Tally output has been digitized using GSheets.  
Finalizing the ER-schema an empty SQLite DB can be created.  
(Using DBschema - application file: DB_Schema_Arzana2018.dbs,  Schema garph: DB_Schema_Arzana2018.jpg,  output: DB_Schema_Arzana2018.sql)
Different Rnotebooks take care of the steps required to transfer the data to the relational DB  
  
A1) "2018ArzanaPinusSurvey_Preprocessing.Rmd" (http://rpubs.com/scotti/455450)  
+ raw verifications   

A2) "2018ArzanaPinusSurvey_TransferTallyToTheDB.Rmd" (http://rpubs.com/scotti/455442)
+ provide complementary info
+ prepare coherent and complete DB-shaped tables
+ create the empty DB file
+ populate the DB

MEMOS
* process stems base arching measures