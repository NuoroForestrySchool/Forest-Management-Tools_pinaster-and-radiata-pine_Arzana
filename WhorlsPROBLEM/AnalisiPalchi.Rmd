---
title: "2018, Arzana - Pinus plantation - sample stems survey"
subtitle: "Transfer tally data to RDBMS"
author:
- name: Matteo Mura
  affiliation: NuoroForestrySchool
abstract: |
  DB schema structure is prepared in DBSchema and saved as image and as SQL script. The procedure reads input data from GoogleSheets, completes the information, produces tables ready for the validating DB, reads the SQL script creating the SQLite DB and appends the tables.  
  Repo: https://gitlab.com/NuoroForestrySchool/Forest-Management-Tools_pinaster-and-radiata-pine_Arzana.git  
keywords: "uploading SQLite DB"
date: "`r format(Sys.time(), '%B %d, %Y')`"
output:
  html_document:
    df_print: paged
  html_notebook: default
  pdf_document: default
  word_document: default
---

```{r}
# library(DBI)
# library(RSQLite)
library(dplyr)
library(tidyverse)
```
```{r}
db_fc <- "ForestMeasures.sqlite" %>%
  src_sqlite()

Rotelle <- tbl(db_fc, "Rotelle") %>% as_tibble()

FustiCampioneCONAdF <- unique(Rotelle$KeyID)

FustiCampioneEtAdF <- tbl(db_fc, "FustiCampioneEtAdF") %>%
  as_tibble() %>% filter(KeyID %in% FustiCampioneCONAdF)

Palchi <- tbl(db_fc, "Palchi") %>% as_tibble() %>% filter(KeyID %in% FustiCampioneCONAdF)

exp_rings <- Rotelle %>% 
  inner_join(Palchi, by = c("KeyID" = "KeyID")) %>% 
  filter(distSuolo.x < distSuolo.y) %>% 
  group_by(KeyID, cls_h) %>% 
  summarise(n=n()) %>% 
  inner_join(Rotelle)

pdf("analisiPalchi.pdf", 8, 11)
for (k in FustiCampioneCONAdF[1:3]) {
  table <- Palchi %>%
    as_tibble() %>%
    filter(KeyID == k)
  
  rot <- Rotelle %>% 
    filter(KeyID == k)
  
  exp_r <- exp_rings %>% 
    filter(KeyID == k)
  
  gr <- ggplot(table, aes(x = ID_palco, y = distSuolo)) +
    geom_line(size = 0.2) +
    geom_point() + 
    geom_text(aes(label = diamRamoGrosso), nudge_x = -0.5, size = 3) +
    geom_text(aes(label = numeroRami), nudge_x = 0.5, size = 3) +
    geom_text(aes(label = stato), nudge_x = 1.5, size = 2) +
    geom_text(mapping = aes(x = 0.5, y = distSuolo, label = n), data = exp_r, size = 3) +
    ggtitle(paste("ID_fusto:", k)) +
    xlab("ID_palco") +
    ylab("Distanza dal suolo") +
    scale_x_continuous(breaks = 1:max(table$ID_palco)) +
    scale_y_continuous(breaks = rot$distSuolo, labels = rot$cls_h, limits = c(0,NA)) +
    theme_bw()
  print(gr)
}
dev.off()
```


