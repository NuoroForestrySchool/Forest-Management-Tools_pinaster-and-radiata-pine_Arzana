---
title: "Exploring TapeR details"
author: "rs"
date: "1/23/2021"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# TapeR_FIT_LME.f  - Value ... extending details
List of model properties

$fit.lme	
Summary of the fitted lme model.
(commented elements, 
  from 'nlmeObject' help,
  <n> = num.of.measured d_sez, <n_Id> = num.of.stems)
  
 $ groups      :'data.frame':	<n> obs. of  1 variable:
    $ Id: Factor w/ <n_Id> levels                      # from input stem-Id
    
 $ fitted      : num [1:<n>, 1:2]  ...                 
  ..- attr(*, "dimnames")=List of 2
    ..$ : chr [1:<n>] "1" "2" "3" "4" ...
    ..$ : chr [1:2] "fixed" "Id"
    # [, 1] fixed = population fix. e. (corresponding to the fixed effects only)
    # [, 2] Id    = (increasing levels of grouping) stem specific estimate
    # NOTE - These estimates are produced using all available d_sez measures!!
 $ residuals   : num [1:<n>, 1:2] ...
  ..- attr(*, "dimnames")=List of 2
  ..$ : chr [1:<n>] "1" "2" "3" "4" ...
  ..$ : chr [1:2] "fixed" "Id"
  ..- attr(*, "std")= num [1:<n>]  ...   # (NOT documented: std of the estimate?)
    # [, 1] fixed = population residuals (only fixed effects)
    # [, 2] Id    = (increasing levels of grouping) stem specific residual
    
(full list)
 $ modelStruct :List of 1
  ..- attr(*, "settings")= num [1:4] 0 0 1 0
  ..- attr(*, "class")= chr [1:3] "lmeStructInt" "lmeStruct" "modelStruct"
  ..- attr(*, "pmap")= logi [1:10, 1] TRUE TRUE TRUE TRUE TRUE TRUE ...
  .. ..- attr(*, "dimnames")=List of 2
  ..- attr(*, "fixedSigma")= logi FALSE
 $ dims        :List of 5
 $ contrasts   : Named list()
 $ coefficients:List of 2
 $ varFix      : num [1:5, 1:5] 2.616 2.077 1.748 1.141 0.275 ...
  ..- attr(*, "dimnames")=List of 2
 $ sigma       : num 1.11
 $ apVar       : num [1:11, 1:11] 0.00702 0.0063 0.0057 0.00363 0.0132 ...
  ..- attr(*, "dimnames")=List of 2
  ..- attr(*, "Pars")= Named num [1:11] 2.62 2.44 2.25 1.97 3.92 ...
  .. ..- attr(*, "names")= chr [1:11] "reStruct.Id1" "reStruct.Id2" "reStruct.Id3" "reStruct.Id4" ...
  ..- attr(*, "natural")= logi TRUE
 $ logLik      : num -5255
 $ numIter     : NULL
 $ groups      :'data.frame':	3087 obs. of  1 variable:
 $ call        : language lme.formula(fixed = y ~ X - 1, random = list(Id = pdSymm(~Z - 1)), method = "ML",      control = lmeControl(msMax| __truncated__
 $ terms       :Classes 'terms', 'formula'  language y ~ X - 1
  .. ..- attr(*, "variables")= language list(y, X)
  .. ..- attr(*, "factors")= int [1:2, 1] 0 1
  .. .. ..- attr(*, "dimnames")=List of 2
  .. ..- attr(*, "term.labels")= chr "X"
  .. ..- attr(*, "order")= int 1
  .. ..- attr(*, "intercept")= int 0
  .. ..- attr(*, "response")= int 1
  .. ..- attr(*, ".Environment")=<environment: 0x55d137b0e520> 
  .. ..- attr(*, "predvars")= language list(y, X)
  .. ..- attr(*, "dataClasses")= Named chr [1:2] "numeric" "nmatrix.5"
  .. .. ..- attr(*, "names")= chr [1:2] "y" "X"
 $ method      : chr "ML"
 $ fitted      : num [1:3087, 1:2] 38 34.5 30.6 27.3 26.5 ...
  ..- attr(*, "dimnames")=List of 2
 $ residuals   : num [1:3087, 1:2] -8.98 -7.47 -6.56 -8.35 -7.47 ...
  ..- attr(*, "dimnames")=List of 2
  ..- attr(*, "std")= num [1:3087] 1.11 1.11 1.11 1.11 1.11 ...
 $ fixDF       :List of 2
  ..- attr(*, "assign")=List of 1
  ..- attr(*, "varFixFact")= num [1:5, 1:5] 0.391 -0.234 -0.705 0.834 1.103 ...
 $ na.action   : NULL
 $ data        : NULL
 - attr(*, "class")= chr "lme"

# Extracting model fitting estimates
```{r predicted}
f.predict <- function(mod) {
  tibble(
     Id      = mod$fit.lme$groups$Id %>% as.character(),
     fitted  = mod0$fit.lme$fitted[,1],
     f.Id    = mod$fit.lme$fitted[,2],  
     res.fit = mod$fit.lme$residuals[,1],
     res.Id  = mod$fit.lme$residuals[,2],
     obs = f.Id + res.Id
      )
}

```
 
 