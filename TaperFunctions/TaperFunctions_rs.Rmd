---
title: 'Taper functions: testing and development'
author:
- affiliation: NuoroForestrySchool
  name: Matteo Mura
date: "`r format(Sys.time(), '%B %d, %Y')`"
output:
  html_document:
    df_print: paged
keywords: taper functions
subtitle: Applying pre-existent functions and fitting new ones
abstract: |
  Assessment of taper functions collected by the students during their bibliographic research and fitting ad hoc functions using the data collected in the field.
---
# Load required libraries
```{r}
library(plyr)
library(dplyr)
library(tidyverse)
library(TapeR)
```

# First test using TapeR package
```{r}
getwd()
load("TaperFunctions.RData")
aaa_test <- ProfiliFustiPrincipali %>%
  right_join(select(FustiCampioneEtAdF, KeyID, h_ipso, ID_fustoCampione))

aaa_test$ID_fustoCampione <- as.numeric(aaa_test$ID_fustoCampione)
aaa_test$d_sez <- as.numeric(aaa_test$d_sez)

aaa_test <- aaa_test %>%
  select(ID_fustoCampione, d_sez, distSuolo, h_ipso)

plot(x = aaa_test$distSuolo/aaa_test$h_ipso, y = aaa_test$d_sez, pch = as.character(aaa_test$ID_fustoCampione), 
       xlab="Relative height (m)", ylab="Diameter (cm)")

Kublin <- TapeR_FIT_LME.f(Id = aaa_test$ID_fustoCampione, 
                x = aaa_test$distSuolo/aaa_test$h_ipso,
                y = aaa_test$d_sez,
                knt_x = c(0.0, 0.1, 0.75, 1.0),
                ord_x = 4,
                knt_z = c(0.0, 0.1, 1.0),
                ord_z = 4)
```

# Testing taper function from paper ID10
![Function tested](TaperFunction_ID10.jpg)
```{r}
# Testing functions
parameters <- tribble(
  ~param,      ~fnct,    ~value,
  "a0",        "FMOLS",  0.989, 
  "a1",        "FMOLS",  0.963, 
  "a2",        "FMOLS",  0.0458,
  "b1",        "FMOLS",  0.367, 
  "b2",        "FMOLS", -0.335, 
  "b3",        "FMOLS",  0.519, 
  "b4",        "FMOLS",  0.847, 
  "b5",        "FMOLS",  0.0178,
  "b6",        "FMOLS", -0.0265,
  "a0",        "MM3",    1.05,  
  "a1",        "MM3",    0.943, 
  "a2",        "MM3",    0.0473,
  "b1",        "MM3",    0.362, 
  "b2",        "MM3",   -0.691, 
  "b3",        "MM3",    0.585, 
  "b4",        "MM3",    1.13,  
  "b5",        "MM3",    0.0227,
  "b6",        "MM3",    0.0581
)

d_sez <- function(h_sez, d_130, h_ipso, FN = "FMOLS") {
  FN <- unique(FN)
  stopifnot(length(FN)==1, FN %in% parameters$fnct)
  fpar <- parameters %>% filter(fnct == FN)
  for(i in 1:nrow(fpar)) assign(fpar$param[i], fpar$value[i])
  q <- h_sez/h_ipso
  w <- 1-q^(1/3)
  x <- w/(1-(1.3/h_ipso)^(1/3))
  d <- a0*d_130^a1*h_ipso^a2*x^
      (b1*q^4+
       b2*(1/exp(d_130/h_ipso))+
       b3*x^0.1+
       b4*(1/d_130)+
       b5*h_ipso^w+
       b6*x)
}

npp <- 6 # Number of Profiles per Plot

aaa_test_ID10 <- ProfiliFustiPrincipali %>%
  right_join(select(FustiCampioneEtAdF, KeyID, h_ipso, d_130, ID_fustoCampione)) %>%
  filter(ID_fustoCampione <= npp*2) %>% 
  arrange(ID_fustoCampione)

output <- aaa_test_ID10 %>%
  mutate(FN = "obs") %>% 
  bind_rows(mutate(aaa_test_ID10, FN = "FMOLS", 
                   d_sez = d_sez(distSuolo, d_130,
                                 h_ipso, FN))) %>%
  bind_rows(mutate(aaa_test_ID10, FN = "MM3", 
                   d_sez = d_sez(distSuolo, d_130,
                                 h_ipso, FN)))
for(p in seq(1,max(output$ID_fustoCampione), npp)){
  plt <- output %>%
    filter(between(ID_fustoCampione, p, p+npp-1)) %>% 
      ggplot(aes(distSuolo, d_sez)) +
        geom_point(aes(1.3, d_130)) +
        geom_line(aes(linetype = FN)) +
        scale_linetype_manual(values=
              c("twodash", "dotted", "solid")) +
        facet_wrap(vars(ID_fustoCampione), 
                   ncol = 3, scales = "free")
  print(plt)
}
```

```{r echo=FALSE, eval=FALSE}
rmarkdown::render("TaperFunctions/TaperFunctions_rs.Rmd", "pdf_document")
```


